from re import I
import discord
import asyncio
import os
import time
import json
import logging
import copy
import queue
import threading
import subprocess
from discord.client import Client
import motor.motor_asyncio
import bson
import youtube_dl
import urllib.parse

from youtube_dl.utils import limit_length

logging.basicConfig(level=logging.INFO)

emoji_alphabet = {
    "b": "\N{Regional Indicator Symbol Letter B}",
    "r": "\N{Regional Indicator Symbol Letter R}",
    "u": "\N{Regional Indicator Symbol Letter U}",
    "h": "\N{Regional Indicator Symbol Letter H}",
    "thumbsdown": "\N{Thumbs Down Sign}"
}

supported_filters = {
    "tremolo": {
        "string": "tremolo=d={depth}:f={frequency}",
        "default_values": {
            "depth": "0.5",
            "frequency": "5"
        },
        "type": "multiple"
    },
    "pingpong": {
        "string": "apulsator=mode={mode}:hz={frequency}",
        "default_values": {
            "mode": "sine",
            "frequency": "0.5"
        },
        "type": "multiple"
    },
    "vibrato": {
        "string": "vibrato=d={depth}:f={frequency}",
        "default_values": {
            "depth": "0.5",
            "frequency": "5"
        },
        "type": "multiple"
    },
    "volume": {
        "string": "volume=volume={}",
        "type": "single"
    },
    "reverse": {
        "string": "areverse",
        "type": "boolean"
    },
    "pad": {
        "string": "apad=pad_len={}",
        "type": "single"
    },
    "trim": {
        "string": "atrim=start={start}:end={end}",
        "default_values": {
            "start": "2",
            "end": "10",
        },
        "type": "multiple"
    },
    "pitchtempo": {
        "string": "rubberband=tempo={tempo}:pitch={pitch}:pitchq=consistency",
        "default_values": {
            "tempo": 1,
            "pitch": 1
        },
        "type": "multiple"
    },
    "echo": {
        "string": ("aecho=in_gain=1.0:"
                   "out_gain={out_gain}:"
                   "delays={delays}:"
                   "decays={decays}"),
        "default_values": {
            "out_gain": "0.7",
            "delays": "30",
            "decays": "0.7"
        },
        "type": "multiple"
    }
}


class AudioPlayer:
    def __init__(self, client, voice_client):
        """The audio player, queue system and parser for voice clip commands

        This class houses a command queue and methods for manipulating and
        reading that queue.

        Args:
            client (Bot): Used for interfacing with the core bot functions
            voice_client (VoiceClient): Used for interfacing with the discord
                                        voice API.

        """
        self.client = client
        self.queue = queue.Queue()
        self.voice_client = voice_client
        self.paths = {
            "yt_clip": client.CFG.get("ytclipspath"),
            "sound_file": client.CFG.get("soundpath"),
            "trimmed_clip": client.CFG.get("trimmedpath")
        }
        self.ap = client.CFG.get("randomAggregationPipeline")

    async def parse_filters(self, message):
        """Turns our filter string syntax to a string suitable for ffmpeg
        Beware of the dragons... I honestly don't fully understand how this
        shit works, but it does. I probably wrote it like three years ago. I'll
        try and document it inline... If you need to debug anything, or change
        anything in the syntax, GLHF.

        The filter syntax works like so:
            +command[filter(parameter1=value,parameter2=value2)]
        filter is OUR specified filter name (a key in the supported_filters
        dict)

        Args:
            message (str): The full command string or from the first "[" in the
                           command string. It doesn't really matter as long as
                           the "[" is present, otherwise it'll fail. So don't
                           trigger this unless they're present.

        Returns:
            filter_string (str): A string which can be passed straight to
            ffmpegs "-f" parameter.
        """
        # Give us something loopable
        filter_list = message.replace(" ", "")[
                message.index("[")+1:
                message.index("]")
                ].strip(")]").split("),")

        filter_string_list = []
        for f in filter_list:
            # Remove the parentheses, they're only used to delimit commands
            f = f.strip(")")
            # Get the name of the effect that we want to look up in the
            # supported_filters dictionary (from here on written as s_f)
            name = f.split("(")[0]

            # Don't continue if the filter does't exist in s_f
            if name in supported_filters:
                # Split away the filter name, and split on comma so we get
                # potential multiple parameters
                params = f.split("(")[1].split(",")
                if supported_filters[name]["type"] == "boolean":
                    # If the type is boolean it just needs to present in the
                    # return string. The "areverse" filter is an example.
                    filter_string_list.append(
                        supported_filters[name]["string"])
                else:
                    # If it isn't a boolean, parse potential parameters
                    # There are named parameters
                    if "=" in f:
                        # deepcopy the default values from the specific filter
                        # to make them immutable, and then change the values
                        # that have been specified.
                        param_dict = copy.deepcopy(
                            supported_filters[name]["default_values"])
                        for p in params:
                            if "=" in p:
                                # This makes sure that any potential misspelled
                                # or incorrectly specified parameters that will
                                # try to be formatted into the filter string

                                # Change potential specified default values
                                # in "param_dict" if the name of the parameter
                                # exists in there, set the value.
                                if p.split("=")[0] in param_dict:
                                    param_dict[
                                        p.split("=")[0]
                                        ] = p.split("=")[1].strip(")")
                        filter_string_list.append(
                            supported_filters[name]["string"].format(
                                **param_dict
                                ))
                    # I honestly dont' understand this, or well. I don't want
                    # to. It works...
                    elif len(params) > 0:
                        params[0] = params[0].strip(")")
                        filter_string_list.append(
                            supported_filters[name]["string"].format(
                                params[0]
                                ))
        # Sure
        return ",".join(filter_string_list)

    async def play_clip_preview(self, filename, start, end):
        """Called when a preview of a youtube clip snippet is requested
        Pretty much just adds it to the queue with the proper "yt_clip" type.

        Probably an unneccesary function, maybe attempting to DRY up the code?

        *shrugs* IDFK

        """
        await self.clear_queue()
        await self.add_to_queue(filename, {}, "yt_clip", start, end,
                                {"mean_volume": "0 dB", "max_volume": "0 dB"})
        self.voice_client.voice_api.stop()
        await self.start_queue()

    async def start_queue(self):
        """Ran by you when the queue is "filled" to kickstart command playback
        """
        if not(self.queue.empty()):
            payload = self.queue.get()
            filter_string = payload["filters"]
            if payload["cached_name"]:
                path = self.paths["trimmed_clip"] + payload["cached_name"]
            else:
                path = self.paths[payload["type"]] + payload["voice_command"]
            if payload["type"] == "yt_clip" and payload["cached_name"] is None:
                start = payload["start"]
                end = payload["end"]
                if start is None:
                    start = "0"
                if "atrim" in filter_string:
                    start_index = (filter_string.index("atrim=start=") +
                                   len("atrim=start="))
                    try:
                        e_index = filter_string.index(":", start_index)
                    except ValueError:
                        e_index = len(filter_string)
                    if "end=" in filter_string[e_index:]:
                        try:
                            ee_index = filter_string.index(",", e_index +
                                                           len(":end="))
                        except ValueError:
                            ee_index = len(filter_string)
                        try:
                            end = str(min(float(start) +
                                      max(0, float(filter_string[
                                                                e_index +
                                                                len(":end="):
                                                                ee_index])
                                          ), float(end)))
                        except TypeError:
                            end = str(float(start) +
                                      max(0, float(filter_string[
                                                                e_index +
                                                                len(":end="):
                                                                ee_index])))
                    start = str(float(start) +
                                max(0, float(filter_string[start_index:e_index]
                                             )))
                    filter_string = (filter_string[:start_index -
                                     len("atrim=start=")] +
                                     filter_string[ee_index+1:])
                init_atrim = f"atrim=start={start}"
                if end:
                    init_atrim += f":end={end}"
                if filter_string:
                    filter_string = (init_atrim + ","
                                     + filter_string)
                else:
                    filter_string = init_atrim
            gain = 0
            if float(payload["gc"]["max_volume"].split()[0]) < -2:
                vol = float(payload["gc"]["mean_volume"].split()[0])
                gain = self.client.CFG.get("targetGain") - vol
            source_options = ('-preset ultrafast -filter_complex '
                              f'volume={gain}dB,alimiter')
            if filter_string:
                source_options += "," + filter_string
            source = discord.FFmpegOpusAudio(
                        path,
                        options=source_options)
            print(source_options)
            self.voice_client.voice_api.play(source, after=self._after)

    def _after(self, error):
        """Wrapper function for calling self.start_queue everytime a command
        finishes playing."""
        coro = self.start_queue()
        fut = asyncio.run_coroutine_threadsafe(coro, self.client.client.loop)
        try:
            fut.result()
        except(Exception):
            pass

    async def clear_queue(self):
        with self.queue.mutex:
            self.queue.queue.clear()

    async def parse_sound_command(self, cstr: str):
        """Parses a sound command and returns the filter and sound path.

        Args:
            cstr: A string containing a single voice command, either
                    prefixed with a SoundPrefix or not

        Returns:
            A dictionary containing parameters that can be passed on to
            AudioPlayer.add_to_queue().

        Example:
            Args:
                cstr: "+lol"

            Return:
                {
                    "name": "lol.mp3",
                    "filters": {},
                    "type": "sound"
                }
        """
        filters = {}
        if not cstr:
            return None
        if "[" in cstr:
            filters = await self.parse_filters(cstr)
            name = cstr.split("[")[0]
            if name.startswith("+"):
                name = name[1:]

        else:
            name = cstr.split("+")[-1]
        if name == "r":
            cursor = self.client.sounds.aggregate(self.ap)
            ret = (await cursor.to_list(1))[0]
        else:
            ret = await self.client.sounds.find_one({"name": name},
                                                    {"filename": 1,
                                                     "start": 1,
                                                     "end": 1,
                                                     "type": 1,
                                                     "cached_name": 1,
                                                     "gain_compensation": 1})
        if ret is not None:
            doc = {
                "name": ret["filename"],
                "filters": filters,
                "type": ret.get("type", "sound_file"),
                "start": ret.get("start"),
                "end": ret.get("end"),
                "cached_name": ret.get("cached_name"),
                "gc": ret.get("gain_compensation")}
            return doc
        return None

    async def handle_sound_command(self, message: discord.Message):
        """Resets voice state and queues (sound) command(s) specified in
        content"""

        # Clear voice client so that we can use it again
        self.voice_client.voice_api.stop()
        await self.clear_queue()
        content = message.content
        if content.startswith(self.client.CFG.get("soundprefix")):
            content = content[1:]
            for sound in content.split(self.client.CFG.get("soundprefix")):
                c = await self.parse_sound_command(sound)
                if c:
                    await self.add_to_queue(c["name"],
                                            c["filters"],
                                            c["type"],
                                            c["start"],
                                            c["end"],
                                            c["gc"],
                                            cached_name=c["cached_name"])
                else:
                    await self.client.react(message, ["thumbsdown"])

        await self.start_queue()

    async def add_to_queue(self, filename, filters, audio_type, start,
                           end, gain_compensation, cached_name=None):
        """Adds a sound command to the command queue.

        Puts a dictionary containing information about a command, used when
        playing it in a channel.

        Args:
            filename (str): The filename of the command that we want to add
                            to the queue.

            filters (dict): Contains information about which filters should
                            be applied to the sound command

            audio_type (str): Dictates whether the audio is a file or a
                                snippet of tmp.mp3 (temporary youtube file).
                                "sound" if it's a normal audio file and
                                "yt_clip" if it's a youtube snippet

            start (str): Only used if type is "yt_clip" with a
                                    specified start.

            end (str): Only used if type is "yt_clip" with a
                                    specified end.

            cached_name (str): Only used if "yt_clip" and a "cached_name"
                               field. This means that it is a trimmed youtube
                               video with a pre-trimmed version available.

        """
        self.queue.put({"voice_command": filename,
                        "filters": filters,
                        "type": audio_type,
                        "start": start,
                        "end": end,
                        "gc": gain_compensation,
                        "cached_name": cached_name})


class VoiceClient:
    def __init__(self, client, guild_id):
        """Simple class holding a guild specific voice state and AudioPlayer.
        """
        self.client = client
        self.guild_id = guild_id
        self.audio_player = AudioPlayer(client, self)

    async def connect(self, channel):
        self.channel = channel
        self.voice_api = await channel.connect()

    async def move_to(self, channel):
        await self.voice_api.move_to(channel)


class User:
    def __init__(self, client, member: discord.Member):
        """Abstraction of a discord member, only used when adding or fetching
        a member ('s id) from the database (so far)
        """
        self.client = client
        self.uid = member.id
        self.name = member.name
        self.discriminator = member.discriminator
        if member.is_avatar_animated():
            self.pf = "gif"
        else:
            self.pf = "webp"
        self.pic_asset = member.avatar_url_as(format=self.pf,
                                              size=1024)

    async def get_user(self):
        """Get the users _id from the database (or create a user and return the
        users new _id)
        """
        self.exists = (await self.client.users.find_one(
            {"user_id": self.uid},
            {"_id": 1}))
        if self.exists:
            return self.exists
        else:
            return (await self.create_user())

    async def create_user(self):
        pp = self.client.CFG.get("profilePicPath") + f"{self.uid}.{self.pf}"
        await self.pic_asset.save(pp)
        return (await self.client.users.insert_one({
            "_id": bson.ObjectId(),
            "user_id": self.uid,
            "name": self.name,
            "discriminator": self.discriminator,
            "updated": time.time(),
            "profile_picture": pp
        }))


class CommandHandler:
    def __init__(self, client):
        """Handles all non sound commands, this includes yt clips and CRUDing
        sound commands.
        """
        self.client = client
        self.ytdl_queues = {}

    def ytdl_hook(self, data):
        video_id = data['filename'].split(".")[0].split("/")[-1]
        q = self.ytdl_queues[video_id]
        q.put(data)

    async def is_admin(self, msg: discord.Message):
        roles = [r.name for r in msg.author.roles]
        if "MinFetaBalle2014" in roles:
            return True
        else:
            return False

    async def reload_config(self, message):
        if await self.is_admin(message):
            with open("/app/config.json", "r") as f:
                self.client.CFG = json.load(f)

    async def add_command(self, name, filename, author: discord.Member,
                          gain_comp, start=None, end=None, _type="sound_file"):
        user = User(self.client, author)
        user_id = (await user.get_user())["_id"]
        doc = {
            "name": name,
            "filename": filename,
            "plays": [],
            "added_by": user_id,
            "added": time.time(),
            "gain_compensation": gain_comp,
            "type": _type,
        }
        if start:
            doc["start"] = start
            doc["end"] = end

        result = await self.client.sounds.insert_one(doc)
        return result.inserted_id

    def get_gain_compensation(self, path, queue):
        output = subprocess.run(["ffmpeg", "-i", path, "-vn", "-sn", "-dn",
                                 "-filter:a", "volumedetect",
                                 "-f", "null", "/dev/null"],
                                capture_output=True).stderr.decode("utf-8")

        usable = [l.split("] ")[1] for l in output.split("\n")
                  if l.startswith("[Parsed_volumedetect")]
        values = {}
        keys = ["mean_volume", "max_volume"]
        for i in usable:
            si = i.split(": ")
            if(si[0] in keys):
                values[si[0]] = si[1]
        queue.put(values)

    async def get_gc_async_wrapper(self, path):
        q = queue.Queue()
        t = threading.Thread(target=self.get_gain_compensation,
                             args=(path, q))
        t.start()
        while t.is_alive():
            await asyncio.sleep(0.1)
        return q.get()

    async def addfile(self, message: discord.Message):
        """Adds a new file by attachment. Has full safety checks.

        message.content should be `addfile <name>` where name is specifying the
        new command name, that will be created for the attached sound.
        """
        smc = message.content.split()[1:]
        if len(smc) == 1:
            name = smc[0]
            if message.attachments:
                att: discord.Attachment = message.attachments[0]
                ext = att.filename.split(".")[1]
                if ext in self.client.CFG.get("allowedExtensions"):
                    exists = await self.client.sounds.find_one({"name": name})
                    if not exists:
                        path = self.client.CFG.get("soundpath") \
                               + f"{name}.{ext}"
                        await att.save(path)
                        gain_comp = await self.get_gc_async_wrapper(path)
                        await self.add_command(name,
                                               f"{name}.{ext}",
                                               message.author,
                                               gain_comp)
                        await message.reply(f"Added command {name}")
                    else:
                        await message.reply("Command already exists")
                else:
                    await message.reply(f"extension `{ext}` is not allowed")
            elif len(message.attachments) > 1:
                await message.reply(
                    "This command needs exactly one attachment")
            else:
                await message.reply("This command requires an attachment")
        else:
            await message.reply(
                "This command requires an argument. `addfile <filename>`")

    async def rename_command(self, message: discord.Message):
        """Renames a sound command in the database

        The command itself takes two parameters. The first one is the name
        of the command that you'd like to rename, and the second is the new
        name that you want to assign to the command. These are represented
        smc[1] and smc[2] (smc being Split Message Content)

        """
        smc = message.content[1:].split()
        if len(smc) == 3:
            first_exists = await self.client.sounds.find_one({"name": smc[1]})
            second_exists = await self.client.sounds.find_one({"name": smc[2]})
            if first_exists and not second_exists:
                await self.client.sounds.update_one(
                    {"_id": first_exists["_id"]},
                    {"$set": {"name": smc[2]}})
                await message.reply(f"Renamed command {smc[1]} to {smc[2]}")
        else:
            await message.reply(
                "This command requires two arguments "
                "`rename <old_name> <new_name>`")

    async def remove_command(self, message: discord.Message):
        """Removes a command from the database and the local storage.
        """
        smc = message.content[1:].split()
        is_admin = await self.is_admin(message)
        if is_admin:
            if len(smc) == 2:
                exists = await self.client.sounds.find_one({"name": smc[1]})
                if exists:
                    await self.client.sounds.delete_one({"name": smc[1]})
                    if not exists.get("type") == "yt_clip":
                        os.remove(self.client.CFG.get("soundpath") +
                                  exists["filename"])
                    elif exists.get("cached_name"):
                        os.remove(self.client.CFG.get("trimmedpath") +
                                  exists["cached_name"])

                    await message.reply(f"Removed command `{smc[1]}`")
                else:
                    await message.reply(
                        f"The command `{smc[1]}` doesn't exist")
            else:
                await message.reply("The command requires an argument")

    def get_video_id(self, url):
        parseresult = urllib.parse.urlparse(url)
        if "youtu.be" in parseresult.netloc:
            return parseresult.path[1:]
        qparm = urllib.parse.parse_qs(parseresult.query)
        v = qparm.get("v")
        if v:
            if len(v) == 1:
                return qparm.get("v")[0]
        return None

    def trim_clip(self, vid, start, end, name, _id, queue=None):
        if start is not None:
            command = ["ffmpeg", "-i", "/yt_cache/" + vid +
                       ".mp3", "-ss", start]
            if end is not None:
                command.extend(["-to", end])
            elif start == 0:
                if queue:
                    queue.put(False)
            fullpath = self.client.CFG.get("trimmedpath") + f"{name}.mp3"
            command.extend(["-c", "copy", fullpath])
            subprocess.run(command, capture_output=True)
            self.client.sounds.update_one({"_id": _id},
                                          {"$set": {"cached_name":
                                                    name + ".mp3"}})
            if queue:
                queue.put(fullpath)
        if queue:
            queue.put(fullpath)

    async def cache_video(self, url, vid, message):
        filepath = self.client.CFG.get("ytclipspath")
        if vid in self.ytdl_queues:
            await message.reply(
                "Literally punch"
                "<@514738328051122187> in the face. He can't "
                "fucking code for shit... Just ask him to rewrite "
                "the `preview_url` and the `save_url_clip` method"
                ". There's probably another download ongoing of "
                "the same video. (you impatient fuck). "
                "https://media.discordapp.net/attachments/"
                "457668043313774594/846750137052954624/unknown.png")
            return False
        q = queue.Queue()
        self.ytdl_queues[vid] = q
        smsg = await message.reply("Downloading...")
        ytdl_opts = self.client.CFG.get("ytdl_ops")
        ytdl_opts["outtmpl"] = f"{filepath}%(id)s.%(ext)s"
        ytdl_opts["progress_hooks"] = [self.ytdl_hook]
        with youtube_dl.YoutubeDL(ytdl_opts) as ydl:
            print("[YTDL]: Starting download", flush=True)
            t = threading.Thread(target=ydl.download,
                                 args=([url],))
            t.start()
            downloading = True
            while downloading:
                try:
                    yt_message = q.get(block=False)
                    percent = yt_message.get("_percent_str", "100.0%")
                    speed = yt_message.get("_speed_str", "0.0 MiB/s")
                    eta = yt_message.get("_eta_str", "00:00")
                    _id = (yt_message["filename"].split(".")[0].split("/")[-1])
                    size = yt_message.get("_total_bytes_str", "0b")
                    content = (f"Downloading {_id}: {speed} "
                               f"ETA: {eta}. Size: {size}"
                               f"{percent} done")
                    await smsg.edit(content=content, supress=False)
                    if "_eta_str" in yt_message:
                        seta = yt_message["_eta_str"].split(":")
                        eta_seconds = 0
                        for pos, i in enumerate(seta, start=0):
                            try:
                                eta_seconds += int(i) * (60**pos)
                            except ValueError:
                                eta_seconds = 0
                    else:
                        eta_seconds = 0
                    if yt_message["status"] == "finished":
                        downloading = False
                    else:
                        if eta_seconds > 0 and eta_seconds < 60:
                            await asyncio.sleep(0.4)
                        elif eta_seconds > 60:
                            await asyncio.sleep(1)
                except queue.Empty:
                    if not t.is_alive():
                        downloading = False
                        del self.ytdl_queues[vid]
        await smsg.edit(content="Done downloading command.",
                        supress=False)
        return True

    async def save_url_clip(self, message: discord.Message):
        """Saves a youtube video with an optional start and end timestamps.
        Args:
            message (discord.Message): A message object with a content
                                       containing a yt url and optional end and
                                       start timestamps.
        """

        smc = message.content.split()
        if len(smc) >= 3:
            url = smc[1]
            name = smc[2]

            try:
                start = smc[3]
            except IndexError:
                start = 0

            try:
                end = smc[4]
            except IndexError:
                end = None

            exists = await self.client.sounds.find_one({"name": name})
            if exists:
                await message.reply(
                    f"A command with name `{name}` already exists")
                return
            filepath = self.client.CFG.get("ytclipspath")
            file_name_list = [i.split(".")[0] for i in os.listdir(filepath)]
            vid = self.get_video_id(url)
            if vid:
                if vid not in file_name_list:
                    status = await self.cache_video(url, vid, message)
                    if not status:
                        await message.add_reaction("\N{Thumbs Down Sign}")
                        return
                await message.channel.trigger_typing()
                path = self.client.CFG.get("ytclipspath") + vid + ".mp3"
                gc = await self.get_gc_async_wrapper(path)
                _id = await self.add_command(name, vid + ".mp3",
                                             message.author, gain_comp=gc,
                                             start=start, end=end,
                                             _type="yt_clip")

                q = queue.Queue()
                trim_thread = threading.Thread(target=self.trim_clip,
                                               args=(vid, start, end, name,
                                                     _id, q))
                trim_thread.start()
                while trim_thread.is_alive():
                    await asyncio.sleep(0.2)
                res = q.get()
                if(res):
                    gc = await self.get_gc_async_wrapper(res)
                    await self.client.sounds.update_one({"_id": _id}, {
                        "$set": {
                            "gain_compensation": gc
                        }})

                await message.reply(f"Added command `{name}`")
            else:
                await message.reply("Input URL has to be a youtube URL. "
                                    "Like so /fromyt "
                                    "https://www.youtube.com/watch?v=somevid "
                                    "<name> [start] [end]")
        else:
            await message.reply("This command requires at least two parameters"
                                ". /fromyt <url> <name> [start] [end]")

    async def preview_url(self, message):
        """Plays a snippet of a youtube url"""
        smc = message.content.split()
        if len(smc) >= 2:
            url = smc[1]
            try:
                start = smc[2]
            except IndexError:
                start = "0"
            try:
                end = smc[3]
            except IndexError:
                end = None
            vid = self.get_video_id(url)
            if vid:
                filepath = self.client.CFG.get("ytclipspath")
                file_name_list = [i.split(".")[0] for i in os.listdir(filepath)]
                if vid not in file_name_list:
                    await self.cache_video(url, vid, message)

                vc = await self.client.get_voice_client(message)
                await vc.audio_player.play_clip_preview(vid + ".mp3", start,
                                                        end)
            else:
                await message.reply(
                    "Input URL has to be a youtube URL. "
                    "Like so /preview https://www.youtube.com/watch?v=somevid "
                    "[start] [end]")
        else:
            await message.reply(
                "This command requires at least two parameters."
                "/preview <url> [start] [end]")


class Bot:
    def __init__(self):
        """The main class for the whole bot. Everything starts here.

        This also houses the important CFG object, which is read from a json
        file, containing all kinds of parameters and paths for different files.
        It also contains the discord authorization token and the mongodb
        connection string.
        """
        with open("/app/config.json", "r") as f:
            self.CFG = json.load(f)
        self.voice_clients = []

        self.client = discord.Client()
        self.client.event(self.on_ready)
        self.client.event(self.on_message)
        self.client.event(self.on_message_edit)

        self.command_handler = CommandHandler(self)

        self.db_connector = motor.motor_asyncio.AsyncIOMotorClient(
                            self.CFG.get("connectionString"))["discord_bot"]
        self.plays = self.db_connector["plays"]
        self.sounds = self.db_connector["sounds"]
        self.users = self.db_connector["users"]
        self.voice_clients = {}
        self.command_dict = {
            "addfile": self.command_handler.addfile,
            "rename": self.command_handler.rename_command,
            "delete": self.command_handler.remove_command,
            "preview": self.command_handler.preview_url,
            "fromyt": self.command_handler.save_url_clip,
            "reload_cfg": self.command_handler.reload_config
        }

    def run(self):
        self.client.run(self.CFG.get("discordToken"))

    async def react(self, msg: discord.Message, lst):
        for emoji_name in lst:
            e = emoji_alphabet.get(emoji_name)
            if e:
                await msg.add_reaction(e)
                await asyncio.sleep(self.CFG.get("reactionDelay"))
            else:
                for c in emoji_name:
                    e = emoji_alphabet.get(c)
                    if e:
                        await msg.add_reaction(e)
                        await asyncio.sleep(self.CFG.get("reactionDelay"))

    def cleanup_yt_clips(self, yt_files):
        for f in yt_files:
            coro = self.get_sound_by_filename(f)
            fut = asyncio.run_coroutine_threadsafe(coro, self.client.loop)
            exists = fut.result()
            if not exists:
                print(f"Removing {f}")
                os.remove(self.CFG.get("ytclipspath") + f)
            else:
                if exists.get("cached_name") is None:
                    self.command_handler.trim_clip(
                        exists["filename"].split(".mp3")[0],
                        exists.get("start"),
                        exists.get("end"),
                        exists["name"],
                        exists["_id"])

    async def get_sound_by_filename(self, f):
        return (await self.sounds.find_one({"filename": f},
                                           {"filename": 1,
                                            "cached_name": 1,
                                            "start": 1,
                                            "end": 1,
                                            "name": 1}))

    async def fix_gain_compensation(self):
        """Get all commands in entire database and calculate gain compensation
           if the gain_compensation key is missing from the database"""
        commands = await self.sounds.find(
            {"gain_compensation": None}, {"_id": 1,
                                          "filename": 1,
                                          "type": 1,
                                          "cached_name": 1}).to_list(None)
        for c in commands:
            if c.get("cached_name"):
                name = c.get("cached_name")
                path = self.CFG.get("trimmedpath") + name
            elif c.get("type") == "yt_clip":
                name = c.get("filename")
                path = self.CFG.get("ytclipspath") + name
            else:
                name = c.get("filename")
                path = self.CFG.get("soundpath") + name
            print(f"Fixing gain compensation for {path}")
            gc = await self.command_handler.get_gc_async_wrapper(path)
            await self.sounds.update_one({"_id": c["_id"]},
                                         {"$set": {
                                             "gain_compensation": gc
                                         }})

    async def on_ready(self):
        """Triggered when bot is started and connected to the discord API.

        This grabs any sound files in the CFG.soundpath that aren't in the
        database, and adds it to the database (as the Bot User). It also clears
        any possible temporary files (any files that aren't mp3 files) in the
        CFG.ytclipspath folder.
        """
        files = os.listdir(self.CFG.get("soundpath"))
        for f in files:
            exists = await self.sounds.find_one({"filename": f})
            if not exists:
                sfn = f.split(".")
                gc = await self.command_handler.get_gc_async_wrapper(
                    self.CFG.get("soundpath") + f)
                await self.command_handler.add_command(sfn[0], f,
                                                       self.client.user, gc)
        yt_files = os.listdir(self.CFG.get("ytclipspath"))
        threading.Thread(target=self.cleanup_yt_clips,
                         args=(yt_files,)).start()
        await self.fix_gain_compensation()
        print("Bot is running")

    async def on_message_edit(self, old_message, new_message):
        await self.on_message(new_message)

    async def parse_command(self, message: discord.Message):
        """Parses a bot command and calls a function using a lookup table.

        Uses a lookup table to trigger a specific function when a string is
        passed.

        Args:
            message (discord.Message): Passed unchanged to the function
                                       residing by the lookup table.
                                       (self.command_dict)

        """
        smc = message.content.split()
        if smc[0][1:] in self.command_dict:
            await self.command_dict[smc[0][1:]](message)

    async def get_voice_client(self, m: discord.Message):
        """Returns a voice_client instance.
        This method returns a usable voice_client instance, either by
        instancing a new one, or finding an already existing client from the
        self.voice_client dictionary.

        Args:
            m (discord.Message): Passed for accessing Message.reply method and
                                 the Message.author.voice.channel object
        """

        if m.author.voice is None:
            await m.reply("You're not in a voice channel.")
            return False
        if m.guild.id not in self.voice_clients:
            self.voice_clients[m.guild.id] = VoiceClient(self, str(m.guild.id))
            await self.voice_clients[m.guild.id].connect(
                m.author.voice.channel)
        vc = self.voice_clients[m.guild.id]
        if m.author.voice.channel is not vc.channel:
            await vc.move_to(m.author.voice.channel)
        if not vc.voice_api.is_connected():
            await self.voice_clients[m.guild.id].connect(
                m.author.voice.channel)
        return vc

    async def on_message(self, m: discord.Message):
        """Parses incoming messages.
        This method sends the message object to either the self.parse_command
        method or the VoiceClient.audio_player depending on what the prefix is.

        Args:
            m (discord.Message): Used to get a usable voice_client and to
                                 parse a sound command, or to parse a
                                 bot command

        Returns:
            Nothing
        """
        if m.content.startswith(self.CFG.get("soundprefix")):
            vc = await self.get_voice_client(m)
            if(vc):
                await vc.audio_player.handle_sound_command(m)
        elif m.content.startswith(self.CFG.get("commandprefix")):
            await self.parse_command(m)


b = Bot().run()
