FROM python:bullseye
RUN mkdir /app
RUN mkdir /sounds
RUN apt update
RUN apt install ffmpeg -y
COPY requirements.txt /app/requirements.txt
RUN pip3 install -r /app/requirements.txt
COPY main.py /app/main.py
ENTRYPOINT ["python", "-u", "/app/main.py"]
